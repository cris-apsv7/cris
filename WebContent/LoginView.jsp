<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file = "Header.jsp"%>
<title>Login view</title>
</head>
<body>

<table>
<tr>
<td>  ${message}</td>  
<td>  ${user}</td>  
<td>  ${user.id}</td>  
<td>  ${email}</td>        
</tr>
</table>

<h2>Login</h2>
	<form action="LoginServlet" method="post">
		<input type="text" name="email" placeholder="Email">
		<input type="password" name="password" placeholder="Password">
		<button type="submit">Login</button>
	</form>

</table>
</body>
</html>